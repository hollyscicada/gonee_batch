package com.kb.api.service.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.kb.api.persistence.MasterDao;
import com.kb.api.service.push.PushService;

@Service
public class JobService {

	@Inject
	MasterDao MasterDao;
	
	@Inject
	PushService pushService;

	private static final Logger logger = LoggerFactory.getLogger(JobService.class);

	public void sleepEndPushJob() throws IOException {
		List<HashMap<String, Object>> memberList = null;
		memberList = (List<HashMap<String, Object>>) MasterDao.dataListTras("mapper.JobMapper", "sleepEndPushJob", null);

		Gson gson = new Gson();

		if (!StringUtils.isEmpty(memberList) && memberList.size() > 0) {
			int successRecord = 0;
			int failRecord = 0;
			List<HashMap<String, Object>> subList = new ArrayList<HashMap<String, Object>>();
			for (int i = 0; i < memberList.size(); i++) {
				HashMap<String, Object> paramMap = memberList.get(i);
				
				
				paramMap.put("push_title", "자야 할 시간이에요");
				paramMap.put("push_content", paramMap.get("sleep_time_text")+"입니다. 고니와 함께 잠들어보세요.");

				int record = 0;
				try {

					HashMap<String, Object> resMap = pushService.sendPush(paramMap);
					paramMap.put("result", resMap.get("result"));

					record = Integer.parseInt(resMap.get("record").toString());
				} catch (HttpServerErrorException e) {

					paramMap.put("result", e.getResponseBodyAsString());
					logger.error("푸시전송 에러 = {}", paramMap);
				} catch (HttpClientErrorException e) {

					paramMap.put("result", e.getResponseBodyAsString());
					logger.error("푸시전송 에러 = {}", paramMap);
				}
				paramMap.put("record", record);

				if (record > 0) {
					successRecord++;
				} else {
					failRecord++;
				}
				subList.add(paramMap);
			}

			Map<String, Object> logMap = new HashMap<String, Object>();
			ObjectMapper mapper = new ObjectMapper();
			String req_data = mapper.writeValueAsString(subList);

			logMap.put("success", successRecord);
			logMap.put("fail", failRecord);
			logMap.put("req_data", req_data);
			logMap.put("res_data", null);
			logMap.put("type", "PUSH");
			try {
				MasterDao.dataCreateTra("mapper.LogMapper", "create_log", logMap);
			} catch (Exception e) {
				logger.error("로그저장 에러 = {}", logMap);
			}
		}
	}
}
