package com.kb.api.service.push;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.json.simple.JSONObject;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.kb.api.persistence.MasterDao;

@Service
public class PushService {
	@Inject
	MasterDao MasterDao;
	
	public String getPushAccessToken() throws IOException {

		DefaultResourceLoader dr = new DefaultResourceLoader();
		Resource re = dr.getResource("classpath:gonee-1f632-firebase-adminsdk-xg3wa-947461e2e6.json");

		String path = re.getFile().getAbsolutePath();
		String MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
		String[] SCOPES = { MESSAGING_SCOPE };

		GoogleCredential googleCredential = GoogleCredential.fromStream(new FileInputStream(path)).createScoped(Arrays.asList(SCOPES));
		googleCredential.refreshToken();
		
		return googleCredential.getAccessToken();
	}

	public HashMap<String, Object> sendPush(Map<String, Object> paramMap) throws IOException {
		
		HashMap<String, Object> resMap = new HashMap<String, Object>();
		if(StringUtils.isEmpty(paramMap.get("push_title")) || StringUtils.isEmpty(paramMap.get("push_content"))) {
			resMap.put("res", "필수값(push_title or push_content)없음");
			resMap.put("record", 0);
			return resMap;
		}
		
		String title = paramMap.get("push_title").toString();
		String content = paramMap.get("push_content").toString();
		String push_token = paramMap.get("push_token").toString();
		

		DefaultResourceLoader dr = new DefaultResourceLoader();
		Resource re = dr.getResource("classpath:gonee-1f632-firebase-adminsdk-xg3wa-947461e2e6.json");

		String path = re.getFile().getAbsolutePath();
		String MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
		String[] SCOPES = { MESSAGING_SCOPE };

		GoogleCredential googleCredential = GoogleCredential.fromStream(new FileInputStream(path)).createScoped(Arrays.asList(SCOPES));
		googleCredential.refreshToken();

		
		System.out.println("토큰 : " +googleCredential.getAccessToken());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", MediaType.APPLICATION_JSON_VALUE);
		headers.add("Authorization", "Bearer " + googleCredential.getAccessToken());
		JSONObject notification = new JSONObject();
		notification.put("body", content);
		notification.put("title", title);
		
		JSONObject message = new JSONObject();
		message.put("notification", notification);
		message.put("token", push_token);
		
		JSONObject jsonParams = new JSONObject();
		jsonParams.put("message", message);
		
		
		

		HttpEntity<JSONObject> httpEntity = new HttpEntity<JSONObject>(jsonParams, headers);
		RestTemplate rt = new RestTemplate();

		ResponseEntity<String> res = rt.exchange("https://fcm.googleapis.com/v1/projects/gonee-1f632/messages:send", HttpMethod.POST, httpEntity, String.class);
		resMap.put("result", res.getBody());
		
		if (res.getStatusCode() == HttpStatus.OK) {
			resMap.put("record", 1);
		}else {
			resMap.put("record", 0);
		}
		return resMap;
		

	}

}
