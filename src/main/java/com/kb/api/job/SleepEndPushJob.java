package com.kb.api.job;

import javax.inject.Inject;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.kb.api.service.MasterService;
import com.kb.api.service.job.JobService;

@Component
public class SleepEndPushJob {  
	
	@Inject MasterService masterService;
	@Inject JobService jobService;
	
	
	@Scheduled(cron="0 0/1 * * * *", zone = "Asia/Seoul")
	public void sleepEndPushJob() throws Exception{
		jobService.sleepEndPushJob();
	}
}
