package com.kb.api.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * @version : java1.8
 * @author : ohs
 * @date : 2018. 8. 8.
 * @class :
 * @message : API 서버 공통 소스
 * @constructors :
 * @method :
 */
public class Common {
	static Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
	public static String sendPush(String url, String method, String header, HashMap<String, String> param) {
		try {
			
	
		    URL Url = null;
		    Url = new URL(url);
		
		    HttpURLConnection con = (HttpURLConnection) Url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
		    
		    if(!StringUtils.isEmpty(header)) {
		    	con.setRequestProperty("Authorization", "Bearer "+header);
		    }
		    
			//사용자 API KEY
		    con.setRequestMethod(method); 
		    con.setDoOutput(true); 
		    
		    
		    
		    
		    OutputStream os = con.getOutputStream();
		    if(param != null) {
		    	os.write(makeParam(param).getBytes("UTF-8"));
		    }
		    os.flush();
		    os.close();
		
		    int responseCode = con.getResponseCode(); //결과 리턴 200이면 통신성공
		
		    BufferedReader br;
		    if (responseCode == 200) {
		        br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		    } else {
		        br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8"));
		    }
		
		    String inputLine;
		    StringBuffer response2 = new StringBuffer();
		    while ((inputLine = br.readLine()) != null) {
		    	response2.append(inputLine);
		    }

		    String str = response2.toString();
		    JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(str);
			str = gson.toJson(je);
			
			//System.out.println("--------------------      Response Param     ---------------------------");
			//System.out.println(str);
			//System.out.println("------------------------------------------------------------------------");

		    br.close();
		    return response2.toString();
		}catch (Exception e) {
		    e.printStackTrace();
		    return "";
		}
	}
	public static String makeParam(HashMap<String, String> param_map) {
		
		String param = "a=1";
		for(String id : param_map.keySet()) {
			param += "&"+id+"="+param_map.get(id).toString();
		}
		param = param.replaceAll("\"", "");
		//System.out.println("--------------------      Request Param     ---------------------------");
		//System.out.println(param);
		//System.out.println("------------------------------------------------------------------------");
		return param;
	}
	
	
}
