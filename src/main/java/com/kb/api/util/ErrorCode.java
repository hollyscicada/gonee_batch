package com.kb.api.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

public class ErrorCode {
	
    
	public static String errorMap(String key) {
		HashMap<String, String> map = new HashMap();
		map.put("900", "사용자 정보가 존재하지 않습니다.");
		map.put("901", "해당일자에 수면정보가 존재하지 않습니다.");
		map.put("902", "평균 수면정보가 존재하지 않습니다.");
		map.put("903", "수면측정을 한번도 진행하지 않았습니다.");
		map.put("904", "수면유형 정보가 존재하지 않습니다.");
		map.put("905", "어제 수면정보가 존재하지 않습니다.");
		map.put("906", "기존에 가입된 회원입니다.");
		map.put("907", "기상청 API 연동중 에러가 발생하였습니다.");
		map.put("908", "수면 만족도 데이터가 유효하지 않습니다.(question_seq, example_seq) 값을 다시 확인해주세요.");
		map.put("910", "수면점수(밴드 깊은 수면시간)를 채점하는 중 에러가 발생하였습니다.");
		map.put("911", "수면점수(밴드 렘 수면시간)를 채점하는 중 에러가 발생하였습니다.");
		map.put("912", "수면점수(밴드 뒤척음)를 채점하는 중 에러가 발생하였습니다.");
		map.put("913", "수면점수(스누즈)를 채점하는 중 에러가 발생하였습니다.");
		map.put("914", "수면점수(미션 달성률)를 채점하는 중 에러가 발생하였습니다.");
		map.put("915", "수면점수(입면시간 점수)를 채점하는 중 에러가 발생하였습니다.");
		map.put("916", "수면점수(수면중단 점수)를 채점하는 중 에러가 발생하였습니다.");
		map.put("917", "수면점수(총수면시간 점수)를 채점하는 중 에러가 발생하였습니다.");
		map.put("918", "수면점수(총수면시간 점수)를 업데이트 중 에러가 발생하였습니다.");
		map.put("919", "이전에 수면측정을 완료한 수면데이터 입니다.");
		map.put("920", "주간 수면측정 점수가 존재하지 않습니다.");
		map.put("921", "주간 수면측정 수행한 미션이 존재하지 않습니다.");
		map.put("922", "미션이 존재하지 않습니다.");
		map.put("923", "주간 수면점수가 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("924", "주간 수면시작시간이 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("925", "주간 수면총시간이 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("926", "주간 깊은수면시간 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("927", "주간 뒤척임 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("928", "주간 잠에서 꺤횟수 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("929", "주간 수면종료시간이 가장 높은 or 낮은 수면 정보가 존재하지 않습니다.");
		map.put("930", "주간 수면측정한 데이터가 존재하지 않습니다.");
		map.put("931", "계정 생성 7일 이전입니다.");
		map.put("932", "계정 생성 30일 이전입니다.");
		map.put("933", "월간 수면측정한 데이터가 존재하지 않습니다.");
		map.put("934", "나의 유형(성별, 생년, 타입) 존재하지 않습니다.");
		map.put("935", "나의 유형(성별, 생년, 타입) 평균 수면정보가 존재하지 않습니다.");
		map.put("936", "수면측정 데이터를 업데이트하는 도중 에러가 발생하였습니다.(2)");
		map.put("937", "수면측정 데이터를 업데이트하는 도중 에러가 발생하였습니다.(2)");
		map.put("938", "수면만족도 데이터를 저장하는 도중 에러가 발생하였습니다.");
		map.put("939", "수면데시벨 데이터를 저장하는 도중 에러가 발생하였습니다.");
		map.put("940", "수면차트 데이터를 저장하는 도중 에러가 발생하였습니다.");
		map.put("941", "수면측정 데이터를 저장하는 도중 에러가 발생하였습니다.");
		map.put("942", "차트데이터 삭제 도중 에러가 발생하였습니다.");
		map.put("943", "수면측정 데이터를 최초 저장하는 도중 에러가 발생하였습니다.");
		map.put("944", "수면측정 데이터가 존재하지 않습니다.");
		map.put("945", "복용중인 약 정보를 수정하는 도중 에러가 발생하였습니다.");
		map.put("946", "주간레포트 중 수면데이터를 받아오는중 에러가 발생하였습니다.");
		map.put("947", "일상데이터 저장 중 에러가 발생하였습니다.");
		map.put("948", "일상데이터를 조회중 에러가 발생하였습니다.");
		map.put("949", "수면만족도를 이전에 저장한적이 있습니다.");
		map.put("950", "일일레포트 중 수면가이드를 저장하는 과정에서 에러가 발생하였습니다.");
		
		
		return map.get(key);
	}
	public static String find(String key) {
		return errorMap(key);
	}
	
	public static Map<String, Object> makeError(Map<String, Object> map, Exception e){
		if(!StringUtils.isEmpty(ErrorCode.find(e.getMessage()))) {
			map.put("errorCode", e.getMessage());
			map.put("errorMsg", ErrorCode.find(e.getMessage()));
		}else {
			e.printStackTrace();
			map.put("errorCode", "500");
			map.put("errorMsg", e.getMessage());
		}
		return map;
	}
}
