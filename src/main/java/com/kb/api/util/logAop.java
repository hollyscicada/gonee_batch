package com.kb.api.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.auth.AuthenticationException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Aspect
public class logAop {
	
	private static final Logger logger = LoggerFactory.getLogger(logAop.class);

	//해당되는 컨트롤러로 요청이 들어왔을때는 AOP를 무시한다.
	// controller 패키지 안에 있는 파일들은 전부 AOP를 거친다 하지만 뒤에 정의 되어 있는 AjaxController, LoginController은 무시한다.
	// 여기에 작성하지 않으면 전부 무시되지만 밑의 처럼 controller 패키지 자체를 거치게 했는데 그중 무시하고자 하는 파일이 있을경우 아래처럼한다.
	@Around("execution(* com.kb.api.controller..*.*(..)) || execution(* com.kb.admin.controller..*.*(..)) ")
	public Object setMapParamter(ProceedingJoinPoint joinPoint) throws Throwable { //
		// request, response 객체 얻어오기
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
//		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();
		
		// request에 담겨온 url, 파라미터 콘솔에출력
		logger.warn("################################################################################################");
		logger.warn("protocol    :  " + request.getProtocol());
		logger.warn("URL         :  " + request.getRequestURL());
		logger.warn("method      :  " + request.getMethod());
		logger.warn("referer     :  " + request.getHeader("referer"));
		Enumeration params = request.getParameterNames();
		int index = 1;
		while (params.hasMoreElements()) {
			String name = (String) params.nextElement();
			logger.warn("param [" + (index++) + "]   :  " + name + " - " + request.getParameter(name));
		}
		logger.warn("################################################################################################");
		return joinPoint.proceed();
	}
	
	/*
	@Around("execution(org.springframework.http.ResponseEntity com.kb.api.controller..*.*(..)) && "
			+ "("
			+ "!execution(* com.kb.api.controller.login.LoginRestController.login(..)) &&"
			+ "!execution(* com.kb.api.controller.login.LoginRestController.find_pw(..)) &&"
			+ ")")
	public Object setJwt(ProceedingJoinPoint joinPoint) throws Throwable { //
		// request, response 객체 얻어오기
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		int rstToken = new Common().checkToken(request);
		System.out.println(rstToken);
		System.out.println(rstToken);
		System.out.println(rstToken);
		System.out.println(rstToken);
		if(rstToken != 0) {
			ResponseEntity<Object> entity = null;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("success", false);
			map.put("erroMsg", "토큰이 유효하지 않습니다.");
			
			entity = new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
			return entity;
		}else {
			return joinPoint.proceed();
		} 
	}
	*/
	
}
